# Servidor Web 
FROM debian:latest
LABEL author="@edt Enrique Lorente "
LABEL subject="webserver"
LABEL curs="ASIX 2023-2024"
RUN apt-get update
RUN apt-get install -y procps iproute2 iputils-ping tree nmap vim apache2  
RUN mkdir /opt/docker
COPY * /opt/docker
COPY index.html /var/www/html/index.html
WORKDIR /opt/docker
CMD apachectl -k start -X 
EXPOSE 80 
